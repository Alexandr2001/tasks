package task11;

import java.util.Scanner;

public class Days {
    private static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Введите кол-во суток: ");
        int n = scan.nextInt();

        System.out.println("В "+n+" сутках: "+(n*24)+"часов, "+(n*24*60)+"минут");
    }
}
