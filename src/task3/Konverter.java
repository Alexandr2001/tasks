package task3;

import java.util.Scanner;

public class Konverter {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Кол-во рублей: ");
        double rub = scan.nextDouble();
        while (rub < 0) {
            System.out.println("Данные введены не корректно! \nПопробуйте снова");
            rub = scan.nextDouble();
        }
        System.out.print("Курс евро на сег.день: ");
        double euro = scan.nextDouble();
        while (euro < 0) {
            System.out.println("Данные введены не корректно! \nПопробуйте снова");
            euro = scan.nextDouble();
        }

        System.out.printf("%s руб в Евро = %.2f Евро", rub, (rub / euro));
    }
}
