package task9;

import java.util.Scanner;

public class Palindrome {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите слово(число): ");
        String palindrome = scan.nextLine();
        isPalindrome(palindrome);
    }

    private static boolean isPalindrome(String palindrome) {
        String r = "";
        for (int i = palindrome.length() - 1; i >= 0; --i) {
            r += palindrome.charAt(i);
        }
        if (palindrome.equalsIgnoreCase(r)) {
            System.out.println("Это слово(число) является полиндромом");
            return true;
        }
        System.out.println("Это слово(число) не является полиндромом");
        return false;
    }
}
