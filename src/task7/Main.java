package task7;

import java.util.Scanner;

public class Main {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        double n = scan.nextDouble();
        if (n%1 == 0){
            System.out.println("Это число целого типа");
        } else {
            System.out.println("Это число дробного типа");
        }
    }
}
