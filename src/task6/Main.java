package task6;
import java.util.Scanner;
public class Main {
    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        int n = scan.nextInt();
        TableMultiplication(n);
    }

    private static void TableMultiplication(int n) {
        for (int i = 2; i < 11; i++){
            System.out.println(n+" * "+i+" = "+(n*i));
        }
    }
}
