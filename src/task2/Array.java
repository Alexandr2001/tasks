package task2;

import java.util.Scanner;

public class Array {
private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        double [] Array = {1, 5, 100};
        System.out.println(isArray(Array));
    }

private static double isArray(double[] Array) {
        System.out.println("Какой элемент массива вы хотите увеличить? ");
        int element = scan.nextInt();
        while (element < 0 || element > Array.length){
            System.out.println("Этот элемент отсутствует в массиве");
            element = scan.nextInt();
        }
        return (Array[element]*0.1) + Array[element];
    }
}